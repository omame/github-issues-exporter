#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# Build this image
# Intended to be run from local CI or local machine
set -eufo pipefail
export SHELLOPTS	# propagate set to children by default
IFS=$'\t\n'

# check required commands are in place
command -v buildah >/dev/null 2>&1 || { echo 'please install buildah or use an image that has it'; exit 1; }

# replace non-alphumdashes with dashes in the branch name
CI_COMMIT_REF_SLUG="$(echo "${CI_COMMIT_REF_SLUG}" | awk '{gsub(/[^[:alnum:]]/,"-")} 1')-${CI_COMMIT_SHORT_SHA}"
# CONTAINER_IMAGE_TAG="${CI_REGISTRY}/${CI_PROJECT_PATH}:${CI_COMMIT_REF_SLUG}"
CONTAINER_IMAGE_LATEST="${CI_REGISTRY}/${CI_PROJECT_PATH}:latest"

# do it
cd "${CI_PROJECT_DIR}/"
buildah bud -f Dockerfile \
	--no-cache \
	--pull \
	-t "${CONTAINER_IMAGE_LATEST}" \
	.
