#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# Publish this image to a Docker registry
set -eufo pipefail
export SHELLOPTS	# propagate set to children by default
IFS=$'\t\n'

# check required commands are in place
command -v buildah >/dev/null 2>&1 || { echo 'please install buildah or use an image that has it'; exit 1; }

# replace non-alphumdashes with dashes in the branch name
CI_COMMIT_REF_SLUG="$(echo "${CI_COMMIT_REF_SLUG}" | awk '{gsub(/[^[:alnum:]]/,"-")} 1')-${CI_COMMIT_SHORT_SHA}"
CONTAINER_IMAGE_TAG="${CI_REGISTRY}/${CI_PROJECT_PATH}:${CI_COMMIT_REF_SLUG}"
CONTAINER_IMAGE_LATEST="${CI_REGISTRY}/${CI_PROJECT_PATH}:latest"

if [[ "true" == "${CI:-false}" ]]; then
	echo "${CI_JOB_TOKEN}" | buildah login -u "${CI_REGISTRY_USER}" --password-stdin "${CI_REGISTRY}"
else
	echo "Not on CI. Bailing out..."
	exit 42
fi

# do it
cd "${CI_PROJECT_DIR}/"
buildah bud -f Dockerfile \
	--no-cache \
	--pull \
	-t "${CONTAINER_IMAGE_TAG}" \
	-t "${CONTAINER_IMAGE_LATEST}" \
	.

buildah push "${CONTAINER_IMAGE_TAG}"
buildah push "${CONTAINER_IMAGE_LATEST}"
buildah logout "${CI_REGISTRY}"
