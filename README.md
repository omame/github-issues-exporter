# GitHub Issues Exporter
---
Fetch metrics about GitHub issues and exports them in Prometheus format.

## Known issues

The GitHub API has a limit of 1000 issues returned for each query. The exporter runs a query for open issues and another one for closed issues, both sorted by date starting from the newest. Thus, the maximum number of issues that it can monitor is 1000 in open state and 1000 in closed state.
