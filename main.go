package main

import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
	"gitlab.com/omame/github-issues-exporter/internal/api"
	"gitlab.com/omame/github-issues-exporter/internal/config"
)

func main() {
	logrus.SetFormatter(&logrus.TextFormatter{
		DisableTimestamp: false,
	})

	args := config.ParseArgs()
	logrus.SetLevel(logrus.InfoLevel)
	if args.Debug {
		logrus.SetLevel(logrus.DebugLevel)
	}

	client := api.NewClient(args)
	prometheus.MustRegister(client)

	http.Handle(args.MetricsPath, promhttp.Handler())
	http.HandleFunc("/status", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`OK`))
	})
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`<html>
             <head><title>GitHub Issues Exporter</title></head>
             <body>
             <h1>GitHub Issues Exporter</h1>
             <p><a href='` + args.MetricsPath + `'>Metrics</a></p>
             </body>
             </html>`))
	})

	logrus.Println("github issues exporter listening on", args.Address)
	logrus.Fatal(http.ListenAndServe(args.Address, nil))
}
