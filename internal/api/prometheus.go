package api

import (
	"strings"
	"sync"
	"time"

	"github.com/google/go-github/v50/github"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/sirupsen/logrus"
	"gitlab.com/omame/github-issues-exporter/internal/metrics"
)

const (
	namespace = "github_issues"
)

// Describe implements Collector interface.
func (g GitHubClient) Describe(ch chan<- *prometheus.Desc) {
	metrics.BootTime.Describe(ch)
	metrics.ClientErrors.Describe(ch)
	metrics.ClientRequests.Describe(ch)
	metrics.ExporterUp.Describe(ch)
}

// Collect collects GitHub issues metrics
func (c GitHubClient) Collect(ch chan<- prometheus.Metric) {
	metrics.ExporterUp.Set(1)
	now := time.Now()

	var wg sync.WaitGroup
	wg.Add(len(c.repos))
	for _, repo := range c.repos {
		go func(repo string, now time.Time) {
			defer wg.Done()

			openIssues, err := c.getOpenIssuesByRepo(repo)
			if err != nil {
				logrus.Fatalf("error fetching open issues for repo %s: %s", repo, err)
			}
			if len(openIssues) == 0 {
				return
			}
			metrics.Issues.WithLabelValues(repo, "open", "_all").Set(float64(len(openIssues)))
			for _, i := range openIssues {
				age := now.Sub(i.CreatedAt.Time).Hours()
				metrics.IssueOpeningAgeHistogram.WithLabelValues(repo, *i.State).Observe(age)

				lastUpdate := now.Sub(i.UpdatedAt.Time).Hours()
				metrics.IssueLastUpdateAgeHistogram.WithLabelValues(repo).Observe(lastUpdate)

			}

			closedIssues, err := c.getClosedIssuesByRepo(repo)
			if err != nil {
				logrus.Fatalf("error fetching closed issues for repo %s: %s", repo, err)
			}
			if len(closedIssues) == 0 {
				return
			}
			metrics.Issues.WithLabelValues(repo, "closed", "_all").Set(float64(len(closedIssues)))
			for _, i := range closedIssues {
				openAge := now.Sub(i.CreatedAt.Time).Hours()
				metrics.IssueOpeningAgeHistogram.WithLabelValues(repo, *i.State).Observe(openAge)
				if i.ClosedAt != nil {
					closedAge := now.Sub(i.ClosedAt.Time).Hours()
					metrics.IssueClosingAgeHistogram.WithLabelValues(repo).Observe(closedAge)
				}
			}

			issues := append(append([]*github.Issue{}, openIssues...), closedIssues...)

			for _, label := range c.labels {
				labelID := strings.ToLower(strings.Replace(label, " ", "-", -1))
				issuesByLabel := getIssuesByLabel(issues, label)
				openissuesByLabel, closedissuesByLabel, err := splitIssuesByState(issuesByLabel)
				if err != nil {
					logrus.Errorf("Couldn't split issues for label %s: %s", label, err)
				}
				metrics.Issues.WithLabelValues(repo, "open", labelID).Set(float64(len(openissuesByLabel)))
				metrics.Issues.WithLabelValues(repo, "closed", labelID).Set(float64(len(closedissuesByLabel)))
			}

			if len(openIssues) > 0 {
				metrics.OldestIssue.WithLabelValues(repo).Set(getOldestIssue(openIssues))
			}
		}(repo, now)
	}
	wg.Wait()

	metrics.BootTime.Collect(ch)
	metrics.ClientErrors.Collect(ch)
	metrics.ClientRequests.Collect(ch)
	metrics.ExporterUp.Collect(ch)
	metrics.IssueOpeningAgeHistogram.Collect(ch)
	metrics.IssueClosingAgeHistogram.Collect(ch)
	metrics.IssueLastUpdateAgeHistogram.Collect(ch)
	metrics.Issues.Collect(ch)
	metrics.OldestIssue.Collect(ch)

	metrics.IssueOpeningAgeHistogram.Reset()
	metrics.IssueOpeningAgeHistogram.Reset()
	metrics.IssueClosingAgeHistogram.Reset()
	metrics.IssueLastUpdateAgeHistogram.Reset()
}
