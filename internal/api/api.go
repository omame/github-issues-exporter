package api

import (
	"context"
	"fmt"

	"github.com/google/go-github/v50/github"
	"github.com/sirupsen/logrus"

	"gitlab.com/omame/github-issues-exporter/internal/config"
	"golang.org/x/oauth2"
)

type GitHubClient struct {
	client *github.Client
	ctx    context.Context
	labels []string
	repos  []string
}

// NewClient creates a new API client to a GitHub endpoint
func NewClient(args config.Args) GitHubClient {
	ctx := context.Background()
	ts := oauth2.StaticTokenSource(
		&oauth2.Token{AccessToken: args.Token},
	)
	tc := oauth2.NewClient(ctx, ts)
	client := github.NewClient(tc)
	return GitHubClient{
		client: client,
		ctx:    ctx,
		labels: args.Labels,
		repos:  args.Repos,
	}
}

// getOpenIssuesByRepo returns all open issues for a GitHub repo
func (c GitHubClient) getOpenIssuesByRepo(repo string) ([]*github.Issue, error) {
	searchOpts := &github.SearchOptions{
		ListOptions: github.ListOptions{Page: 1, PerPage: 100},
	}
	var allIssues []*github.Issue
	// FIXME: GitHub has a limitation of 1000 issues returned per query
	for {
		issues, resp, err := c.client.Search.Issues(c.ctx, fmt.Sprintf("type:issue is:open sort:created-desc repo:%s", repo), searchOpts)
		if err != nil {
			return nil, err
		}
		logrus.Debugf("page %d of %d", resp.NextPage-1, resp.LastPage)
		allIssues = append(allIssues, issues.Issues...)
		if resp.NextPage == 0 {
			break
		}
		searchOpts.Page = resp.NextPage
	}
	logrus.Debugf("found %d issues", len(allIssues))
	return allIssues, nil
}

// getClosedIssuesByRepo returns all closed issues for a GitHub repo
func (c GitHubClient) getClosedIssuesByRepo(repo string) ([]*github.Issue, error) {
	searchOpts := &github.SearchOptions{
		ListOptions: github.ListOptions{Page: 1, PerPage: 100},
	}
	var allIssues []*github.Issue
	// FIXME: GitHub has a limitation of 1000 issues returned per query
	for {
		issues, resp, err := c.client.Search.Issues(c.ctx, fmt.Sprintf("type:issue is:closed sort:created-desc repo:%s", repo), searchOpts)
		if err != nil {
			return nil, err
		}
		logrus.Debugf("page %d of %d", resp.NextPage-1, resp.LastPage)
		allIssues = append(allIssues, issues.Issues...)
		if resp.NextPage == 0 {
			break
		}
		searchOpts.Page = resp.NextPage
	}
	logrus.Debugf("found %d issues", len(allIssues))
	return allIssues, nil
}

func splitIssuesByState(allIssues []*github.Issue) ([]*github.Issue, []*github.Issue, error) {
	opened := make([]*github.Issue, 0)
	closed := make([]*github.Issue, 0)
	for _, i := range allIssues {
		switch *i.State {
		case "open":
			opened = append(opened, i)
		case "closed":
			closed = append(closed, i)
		default:
			logrus.Debug("invalid issue state: %s", *i.State)
			return nil, nil, fmt.Errorf("invalid issue state: aborting issue splitting")
		}
	}
	return opened, closed, nil
}

func getIssuesByLabel(allIssues []*github.Issue, label string) []*github.Issue {
	res := make([]*github.Issue, 0)
	for _, i := range allIssues {
		for _, l := range i.Labels {
			if *l.Name == label {
				res = append(res, i)
			}
		}
	}
	return res
}

func getOldestIssue(allIssues []*github.Issue) float64 {
	res := allIssues[0]
	for _, i := range allIssues {
		if res.CreatedAt.Sub(i.CreatedAt.Time) > 0 {
			res = i
		}
	}
	return float64(res.CreatedAt.Unix())
}

// func uniqueSlice(s []int) []int {
// 	seen := make(map[int]struct{}, len(s))
// 	j := 0
// 	for _, v := range s {
// 		if _, ok := seen[v]; ok {
// 			continue
// 		}
// 		seen[v] = struct{}{}
// 		s[j] = v
// 		j++
// 	}
// 	return s[:j]
// }
