package config

import (
	"flag"
	"fmt"
	"os"
	"strings"

	"github.com/sirupsen/logrus"
)

// Args contains the command line arguments
type Args struct {
	Address           string
	Debug             bool
	Token             string
	BaseURL           string
	Labels            []string
	labelsString      string
	MetricsPath       string
	Repos             []string
	reposString       string
	RequestsPerSecond int
}

// ParseArgs parses the command line arguments
func ParseArgs() Args {
	var args Args

	flag.BoolVar(&args.Debug, "debug", false, "enable debug level logging")
	flag.StringVar(&args.Address, "address", ":9604", "listening address")
	flag.StringVar(&args.MetricsPath, "metrics", "/metrics", "metrics path")
	flag.StringVar(&args.BaseURL, "base-url", "https://github.com", "the url of the instance (default: https://github.com)")
	flag.StringVar(&args.labelsString, "labels", "", "comma separated list of labels")
	flag.StringVar(&args.reposString, "repos", "", "comma separated list of repositories (must include the owner or the organization)")

	flag.Usage = func() {
		fmt.Printf("Usage of %s:\n", os.Args[0])
		flag.PrintDefaults()
		fmt.Println("\nEnvironment variables")
		fmt.Println("  GITHUB_TOKEN token used to access the GitHub instance")
	}

	flag.Parse()

	if args.reposString == "" {
		logrus.Fatalf("must specify at least one repository")
	}

	args.Token = os.Getenv("GITHUB_TOKEN")
	args.Labels = strings.Split(args.labelsString, ",")
	args.Repos = strings.Split(args.reposString, ",")

	if args.Token == "" {
		logrus.Fatalf("token not defined")
	}

	return args
}
